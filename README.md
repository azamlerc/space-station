# Space Station

This app connects to the [OpenNotify](http://open-notify.org/) API to get the current position of the [International Space Station](https://en.wikipedia.org/wiki/International_Space_Station), and to predict the next time it will pass over the user's current location. 

## Running the app

The app requires Xcode 8 or later, and runs on iOS 10 or later. 

To run the app on a device, you can build and run it using Xcode. You probably want to run it on a real device, since it uses the GPS hardware. You'll need to edit the current target and choose your own signing authority.

When you launch the app, you'll see a map of the world with your current location. 

## Locating the Space Station

The app will call the API to find the current location of the Space Station every fifteen seconds, and drop a pin on the map. Tap on a pin to see the time that the space station passed over that point.

## Finding the next pass

The app calls another API to find when the Space Station will next pass over the current location. It displays a countdown with the number of hours and minutes until the next pass, or a message indicating that it is currently overhead. 

The Space Station orbits the earth once every 92 minutes. Each pass lasts about ten minutes, during which time it is visible from the ground at ten degrees or more above the horizon.

Because the trajectory of the Space Station is somewhat variable (avoiding space deris, etc.) the app will call this API every fifteen minutes.

If the app is not currently running, your device will display a notification when the Space Station is overhead. 

## Screenshots

Countdown:

![countdown](images/countdown.png) 

Overhead:

![overhead](images/overhead.png) 

Notification:

![notification](images/notification.png)