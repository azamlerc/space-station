//
//  DateRange.swift
//  Space Station
//
//  Created by Andrew Zamler-Carhart on 10/14/16.
//  Copyright © 2016 Andrew Zamler-Carhart. All rights reserved.
//

import Foundation

class DateRange: NSObject {
    var start: Date
    var end: Date
    var duration: TimeInterval

    init(start: Date, duration: TimeInterval) {
        self.start = start
        self.end = Date(timeInterval: duration, since: start)
        self.duration = duration
        
        super.init()
    }
    
    func future() -> Bool {
        return start.timeIntervalSinceNow > 0
    }
    
    func current() -> Bool {
        return start.timeIntervalSinceNow <= 0 && end.timeIntervalSinceNow >= 0
    }
    
    func past() -> Bool {
        return end.timeIntervalSinceNow < 0
    }
    
    func contains(date: Date) -> Bool {
        return start.timeIntervalSince(date) <= 0 && end.timeIntervalSince(date) >= 0
    }
    
    override var description: String {
        return "\(start) - \(end)"
    }

}
