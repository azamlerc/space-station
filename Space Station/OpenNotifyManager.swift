//
//  OpenNotifyManager.swift
//  Space Station
//
//  Created by Andrew Zamler-Carhart on 10/14/16.
//  Copyright © 2016 Andrew Zamler-Carhart. All rights reserved.
//
//  This class contains methods for calling the OpenNotify API to retrieve
//  information about the location of the International Space Station.
//


import Foundation
import CoreLocation

class OpenNotifyManager: APIManager {
    
    override init() {
        super.init()
        
        server = "http://api.open-notify.org"
    }
    
    // http://open-notify.org/Open-Notify-API/ISS-Location-Now/
    func currentISSPosition(handler: @escaping (CLLocation?) -> ()) {
        sendRequest(uri: "iss-now.json", params: nil) { json in
            // NSLog("Position: \(json)")

            if let position = json["iss_position"] as? JSONDictionary,
                let latitude = position["latitude"] as? Double,
                let longitude = position["longitude"] as? Double
            {
                let location = CLLocation(latitude: latitude, longitude: longitude)
                handler(location)
            }
        }
    }
    
    // http://open-notify.org/Open-Notify-API/ISS-Pass-Times/
    func nextISSPasses(location: CLLocation, handler: @escaping ([DateRange]) -> ()) {
        let params = ["lat":location.coordinate.latitude,
                      "lon":location.coordinate.longitude]
        sendRequest(uri: "iss-pass.json", params: params) { json in
            // NSLog("Passes: \(json)")
            
            var dateRanges = [DateRange]()
            if let response = json["response"] as? JSONArray {
                for pass in response {
                    if let risetime = pass["risetime"] as? Double,
                        let duration = pass["duration"] as? Double
                    {
                        let date = Date(timeIntervalSince1970: risetime)
                        let dateRange = DateRange(start: date, duration: duration)
                        dateRanges.append(dateRange)
                    }
                }
            }
            handler(dateRanges)
        }
    }
}
