//
//  Extensions.swift
//  Space Station
//
//  Created by Andrew Zamler-Carhart on 10/14/16.
//  Copyright © 2016 Andrew Zamler-Carhart. All rights reserved.
//

import Foundation

// calls the closure in the main queue
public func queue(_ closure:@escaping ()->()) {
    DispatchQueue.main.async(execute: closure)
}

// calls the closure in the background
public func background(_ closure:@escaping ()->()) {
    DispatchQueue.global().async(execute: closure)
}

// calls the closure after the delay
// e.g. delay(5.0) { // do stuff }
public func delay(_ duration:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(duration * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
