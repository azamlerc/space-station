//
//  AppDelegate.swift
//  Space Station
//
//  Created by Andrew Zamler-Carhart on 10/14/16.
//  Copyright © 2016 Andrew Zamler-Carhart. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    var openNotify = OpenNotifyManager()
    var locationManager = CLLocationManager()
    var viewController: ViewController?
    
    var issTimer: Timer?
    let issTimerInterval: TimeInterval = 15
    var nextPassTimer: Timer?
    let nextPassTimerInterval: TimeInterval = 900
    
    var userLocation: CLLocation?
    var issLocation: CLLocation?
    var dateRanges: [DateRange]?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        getISSLocation()

        return true
    }

    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
            case .notDetermined: NSLog("Not determined")
            case .restricted: NSLog("Restricted")
            case .denied: NSLog("Denied")
            case .authorizedAlways: NSLog("Authorized Always")
            case .authorizedWhenInUse: NSLog("Authorized When In Use")
        }
        
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            NSLog("User location: \(location.coordinate)")
            userLocation = location
            
            getNextPasses()
        }
    }
    
    func getISSLocation() {
        openNotify.currentISSPosition { location in
            let firstTime = self.issLocation == nil
            if location != nil {
                NSLog("ISS location: \(location!.coordinate)")
                self.issLocation = location
                
                queue {
                    self.viewController!.addPin(location: location!)
                    if firstTime { self.viewController!.centerMap(location: location!) }
                }
            }
        }
    }
    
    func getNextPasses() {
        if userLocation != nil {
            openNotify.nextISSPasses(location: userLocation!) { dateRanges in
                self.dateRanges = dateRanges
                
                NSLog("Next passes: \(dateRanges)")

                if let date = dateRanges.first?.start {
                    self.scheduleAlert(date: date)
                }
                
                queue {
                    self.viewController!.setNextPasses(dateRanges: dateRanges)
                }
            }
        }
    }
    
    func startTimers() {
        issTimer = Timer.scheduledTimer(withTimeInterval: issTimerInterval, repeats: true) { timer in
            self.getISSLocation()
            self.viewController!.updateCountdown()
        }
    }
    
    func stopTimers() {
        if issTimer != nil {
            issTimer!.invalidate()
            issTimer = nil
        }
    }
    
    func scheduleAlert(date: Date) {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        center.requestAuthorization(options: [.alert]) { (granted, error) in
            if granted {
                let content = UNMutableNotificationContent()
                content.title = "Space Station"
                content.body = "Up in the sky! It's a bird! It's a plane! It's the space station!"
                let interval = date.timeIntervalSinceNow
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: interval, repeats: false)
                let request = UNNotificationRequest(identifier: "NextPass", content: content, trigger: trigger)
                center.add(request)
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        stopTimers()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        startTimers()
    }

    func applicationWillTerminate(_ application: UIApplication) {

    }


}

