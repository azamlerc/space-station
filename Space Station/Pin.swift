//
//  ISSPin.swift
//  Space Station
//
//  Created by Andrew Zamler-Carhart on 10/14/16.
//  Copyright © 2016 Andrew Zamler-Carhart. All rights reserved.
//

import Foundation

import MapKit

class Pin: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
