//
//  ViewController.swift
//  Space Station
//
//  Created by Andrew Zamler-Carhart on 10/14/16.
//  Copyright © 2016 Andrew Zamler-Carhart. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var nextPassLabel: UILabel!
    
    var appDelegate: AppDelegate?
    var pin: Pin?
    var timeFormatter = DateFormatter()
    var nextPasses: [DateRange]?
    
    override func viewDidLoad() {
        timeFormatter.timeStyle = .medium
        timeFormatter.dateStyle = .none
        
        super.viewDidLoad()

        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate!.viewController = self
        
        updateCountdown()
    }

    // from https://www.raywenderlich.com/90971/introduction-mapkit-swift-tutorial
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Pin {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
            }
            return view
        }
        return nil
    }
    
    func addPin(location: CLLocation) {
        let time = timeFormatter.string(from: Date())
        pin = Pin(title: "International Space Station",
                     locationName: time,
                     coordinate: location.coordinate)
        
        mapView.addAnnotation(pin!)
        
        updateCountdown() // this is called about every fifteen seconds
    }
    
    func centerMap(location: CLLocation) {
        let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 10000000, 10000000)
        mapView.setRegion(region, animated: true)
    }

    func setNextPasses(dateRanges: [DateRange]) {
        self.nextPasses = dateRanges
        updateCountdown()
    }
    
    func updateCountdown() {
        if let pass = nextPass() {
            if pass.current() {
                nextPassLabel.text = "Currently overhead!"
            } else {
                let seconds = pass.start.timeIntervalSinceNow
                let totalMinutes = Int(seconds / 60.0)
                
                if totalMinutes == 0 {
                    nextPassLabel.text = "On the horizon…"
                } else if totalMinutes < 60 {
                    let formattedMinutes = format(number: totalMinutes, unit: "minute")
                    nextPassLabel.text = "Next pass in \(formattedMinutes)"
                } else {
                    let hours = Int(totalMinutes / 60)
                    let minutes = totalMinutes - hours * 60
                    
                    let formattedHours = format(number: hours, unit: "hour")
                    let formattedMinutes = format(number: minutes, unit: "minute")

                    nextPassLabel.text = "Next pass in \(formattedHours), \(formattedMinutes)"
                }
            }
        } else {
            nextPassLabel.text = "Loading…"
        }
    }
    
    func format(number: Int, unit: String) -> String {
        return number == 1 ? "1 \(unit)" : "\(number) \(unit)s"
    }
    
    func nextPass() -> DateRange? {
        if nextPasses == nil { return nil }
        for pass in nextPasses! {
            if !pass.past() { // i.e. current or future
                return pass
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

