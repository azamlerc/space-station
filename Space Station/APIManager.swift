//
//  APIManager.swift
//  Space Station
//
//  Created by Andrew Zamler-Carhart on 10/14/16.
//  Copyright © 2016 Andrew Zamler-Carhart. All rights reserved.
//
//  This class contains code for calling a generic API. 
//

import Foundation

public typealias JSONDictionary = [String:Any]
public typealias JSONArray = [JSONDictionary]

public class APIManager: NSObject {
    
    public var server = "http://localhost:80"
    
    public enum HTTPMethod: String {
        case GET = "GET"
        case POST = "POST"
        case PUT = "PUT"
        case DELETE = "DELETE"
    }
    
    // takes a uri, params, method and message
    // handler called with json and other info
    public func sendRequest(uri: String,
                            params: JSONDictionary?,
                            handler: @escaping (JSONDictionary) -> ())
    {
        let url = urlWithParams(uri: uri, params:params)
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if data != nil {
                if let json = (try? JSONSerialization.jsonObject(with: data!, options: [])) as? JSONDictionary {
                    handler(json)
                } else {
                    NSLog("Not json: \(String(data: data!, encoding: String.Encoding.utf8))")
                }
            } else {
                NSLog("Couldn't load: \(url)")
            }
        })
        task.resume()
    }
    
    func urlWithParams(uri: String, params: JSONDictionary?) -> URL {
        var urlString = server + "/" + uri
        if params != nil && params!.count > 0 {
            urlString += "?" + paramString(params: params!)
        }
        let url: URL? = URL(string:urlString)
        return url!
    }
    
    public func paramString(params: JSONDictionary) -> String {
        var keyValues = [String]()
        for (key, value) in params {
            keyValues.append("\(key)=\(value)")
        }
        return keyValues.joined(separator: "&")
    }
    

}
